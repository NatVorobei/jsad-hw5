class Card {
    constructor(post) {
        this.post = post;
        this.cardElement = document.createElement('div');
        this.cardElement.classList.add('card');
        this.render();
    }

    render() {
        this.cardElement.innerHTML = `
          <div class="card_header">${this.post.user.name} <span class="card_email">${this.post.user.email}</span></div>
          <div class="card_title">${this.post.title}</div>
          <div class="card_text">${this.post.body}<span class="delete_btn">🗑</span></div>
        `;
    
        this.cardElement.querySelector('.delete_btn').addEventListener('click', () => this.delete());
    }

    delete() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { 
            method: 'DELETE' 
        })
            .then(response => {
                if (response.ok) {
                    this.cardElement.remove();
                }
            })
            .catch(error => console.log(error));
    }
}

let userPost = document.getElementById('userPost');

function getUsers(){
    return fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .catch(error => console.log(error))
} 

function getPosts(){
    getUsers().then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(posts => {
                posts.forEach(post => {
                    let user = users.find(u => u.id === post.userId);
                    post.user = user;
                    let card = new Card(post);
                    userPost.appendChild(card.cardElement);
                });
            })
            .catch(error => console.log(error));
    })
}

getPosts();